import os
from datetime import date

from .licenses import create_MIT_license


class CreateTemplateFiles:
    def __init__(self, path: str = "") -> None:
        # self.author = author
        # self.tpath = path
        self.path = self._check_path(path)
        self.year = self._get_year()

    def _get_year(self):
        today = str(date.today())
        cut = slice(4)

        return today[cut]

    def _check_path(self, path):
        if not os.path.exists(path):
            raise ValueError("given directory path doesn't exist")
        else:
            return path

    def _normalize(self, x):
        normal = x.lower().replace(" ", "_")

        return normal

    def make_src_directory(self, module_name):
        full_path = os.path.join(self.path, f"src/{self._normalize(module_name)}")
        os.makedirs(full_path)

        files = ["__init__.py", "__main__.py"]

        if os.path.exists(full_path):
            for f in files:
                with open(os.path.join(full_path, f), "w") as file:
                    file.write("")
            return True
        else:
            return None

    def make_readme(self, readme):
        full_path = os.path.join(self.path, readme)

        with open(full_path, "w") as file:
            file.write("")

    def make_requirements(self, requirements: list = None):
        full_path = os.path.join(self.path, "requirements.txt")

        with open(full_path, "w") as file:
            if not requirements:
                file.write("")
            else:
                for req in requirements:
                    file.write(req)
                    file.write("\n")

    def make_license(
        self,
        license_type,
        author: str,
    ):
        full_path = os.path.join(self.path, "LICENSE")

        match license_type:
            case "MIT":
                license = create_MIT_license(self.year, author)
                with open(full_path, "w") as file:
                    file.write(license)
                return license

    def make_gitignore(self, defaults: list):
        full_path = os.path.join(self.path, ".gitignore")

        with open(full_path, "w") as file:
            if not defaults:
                file.write("")
                return None
            for default in defaults:
                file.write(default)
                file.write("\n")

    def make_gitlab_ci(self):
        full_path = os.path.join(self.path, ".gitlab-ci.yml")

        with open(full_path, "w") as file:
            file.write("")

    def make_tests_directory(self):
        full_path = os.path.join(self.path, "tests")
        os.mkdir(full_path)
        os.mkdir(os.path.join(full_path, "integration"))
        os.mkdir(os.path.join(full_path, "unit"))

        if os.path.exists(full_path):
            return True
        else:
            return None

    def make_conftest(self):
        full_path = os.path.join(self.path, "tests")
        unit_path = os.path.join(full_path, "unit")
        integration_path = os.path.join(full_path, "integration")

        if (
            not os.path.exists(full_path)
            and not os.path.exists(unit_path)
            and not os.path.exists(integration_path)
        ):
            raise Exception("directory is non-existent")

        paths = [full_path, unit_path, integration_path]

        for path in paths:
            with open(os.path.join(path, "conftest.py"), "w") as file:
                file.write("import pytest")


if __name__ == "__main__":
    _creator = CreateTemplateFiles()

    _creator
