import argparse
import tomli
import os

from .parser_formatter import CustomHelpFormatter
from .create_toml import ModuleTOMLCreator
from .user_input import UserInput
from .create_files import CreateTemplateFiles
from .utility import (
    check_for_defaults_file,
    get_build_settings,
    normalize_name,
    set_repository_links,
    get_names_from_dict_list,
    combine_names,
)

# not a real comment. created for pipeline
if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=CustomHelpFormatter)
    subparsers = parser.add_subparsers(metavar="command", dest="command")

    # Sub parser declarations
    subparser_create = subparsers.add_parser(
        "create", help="create a new python module template"
    )

    # subparser_create arguments
    subparser_create.add_argument("path", help="path to target directory")

    # subparser_create.add_argument(
    #     "--author", action="store_true", default=False, help="toggle author input"
    # )

    # subparser_create.add_argument(
    #     "--build-system",
    #     action="store_true",
    #     default=False,
    #     help="toggle build-system input",
    # )
    # subparser_create.add_argument(
    #     "--dependencies",
    #     action="store_true",
    #     default=False,
    #     help="toggle dependency input",
    # )
    # subparser_create.add_argument("--repo", help="set repository")

    # subparser_create.add_argument(
    #     "-r", "--requirements", help="items to add to requirements.txt"
    # )
    # subparser_create.add_argument(
    #     "-i", "--ignore", nargs="*", help="items to add to .gitignore"
    # )

    args = parser.parse_args()

    match args.command:
        case "create":
            default = check_for_defaults_file()
            # default = {}

            build_system = default["build"]["build-system"] if default else None
            module_version = default["module"]["version"] if default else None
            authors = default["module"]["authors"] if default else None
            required_python = default["module"]["python-version"] if default else None
            readme = default["file"]["readme"] if default else None
            license = default["file"]["license"] if default else None
            ignore = default["file"]["ignore"] if default else None
            operating_system = default["build"]["operating-system"] if default else None
            repository = default["repository"]["provider"] if default else None
            owner = default["repository"]["owner"]

            get_input = UserInput(
                module_version=module_version,
                authors=authors,
                required_python=required_python,
                readme=readme,
                license=license,
                ignore=ignore,
                repository=repository,
                build_system=build_system,
                operating_system=operating_system,
            )

            inputs = get_input.run()

            authors_for_license = combine_names(
                get_names_from_dict_list(inputs["Authors"])
            )

            # Create the basic files
            ctf = CreateTemplateFiles(path=args.path)

            ctf.make_src_directory(normalize_name(inputs["Module Name"], "_"))
            ctf.make_readme(inputs["Readme"])
            ctf.make_requirements()
            ctf.make_license(inputs["License"], authors_for_license)
            ctf.make_gitignore(inputs["Ignore"])
            ctf.make_gitlab_ci()
            ctf.make_tests_directory()
            ctf.make_conftest()

            homepage, bug_tracker = set_repository_links(
                inputs["Module Name"],
                owner,
                default["repository"]["provider"],
            )

            requires, backend = get_build_settings(inputs["Build System"])

            # Create pyproject.toml
            mtc = ModuleTOMLCreator(
                buildSystem=requires,
                buildSystemBackend=backend,
                moduleName=normalize_name(inputs["Module Name"], "_"),
                moduleVersion=inputs["Module Version"],
                moduleAuthors=inputs["Authors"],
                description=inputs["Description"],
                readme=inputs["Readme"],
                requiredPythonVersion=inputs["Required Python"],
                dependencies=inputs["Dependencies"],
                license=inputs["License"],
                operatingSystem=inputs["Operating System"],
                homepage=homepage,
                bugTracker=bug_tracker,
                savePath=args.path,
            )

            mtc.save()
