import os
import tomli


LICENSE_OPTIONS = {"1": "MIT"}

OS_OPTIONS = {"1": "OS Independent", "2": "Linux"}

BUILD_OPTIONS = {"1": "Hatchling", "2": "Setuptools"}


def check_for_defaults_file():
    homedir = os.path.expanduser("~")
    defaults = os.path.join(homedir, ".mod_template.toml")
    if os.path.exists(defaults):
        with open(defaults, "rb") as file:
            try:
                default = tomli.load(file)
            except tomli.TOMLDecodeError:
                print("not a valid TOML file")

        return default
    else:
        return None


def get_build_settings(name):
    match name:
        case "hatchling":
            requires = ["hatchling"]
            build_backend = "hatchling.build"
            return (requires, build_backend)
        case "setuptools":
            requires = ["setuptools", "setuptools-scm"]
            build_backend = "setuptools.build_meta"
        case _:
            return None

    return (requires, build_backend)


def normalize_name(module_name, separator):
    normal = module_name.lower().replace(" ", separator)

    return normal


def set_repository_links(module_name, owner, provider):
    match provider:
        case "gitlab":
            homepage = (
                f"""https://gitlab.com/{owner}/{normalize_name(module_name, "-")}"""
            )
            bug_tracker = f"""{homepage}/issues"""
        case "github":
            homepage = (
                f"""https://github.com/{owner}/{normalize_name(module_name, "-")}"""
            )
            bug_tracker = f"""{homepage}/issues"""
        case _:
            return None

    return (homepage, bug_tracker)


def get_names_from_dict_list(dict_list):
    collect = []
    for dict in dict_list:
        collect.append(dict["name"])
    return collect


def combine_names(input_list):
    if len(input_list) == 1:
        return input_list[0]
    else:
        output = ", ".join(input_list)
        return output
