import os
from tomli_w import dump, dumps


class ModuleTOMLCreator:
    def __init__(
        self,
        buildSystem: list[str],
        buildSystemBackend: str,
        moduleName: str,
        moduleVersion: str,
        moduleAuthors: list[dict],
        description: str,
        readme: str,
        requiredPythonVersion: str,
        dependencies: list[str],
        license: str,
        operatingSystem: str,
        homepage: str,
        bugTracker: str,
        **kwargs,
    ) -> None:
        self.buildSystem = buildSystem
        self.buildSystemBackend = buildSystemBackend
        self.moduleName = moduleName
        self.moduleVersion = moduleVersion
        self.authors = moduleAuthors
        self.description = description
        self.readme = readme
        self.requiredPythonVersion = requiredPythonVersion
        self.dependencies = dependencies
        self.license = license
        self.operatingSystem = operatingSystem
        self.homepage = homepage
        self.bugTracker = bugTracker

        if "savePath" in kwargs.keys():
            self.savePath = kwargs["savePath"]
        else:
            self.savePath = None

        # print(self.module_name)
        # print(self._preview(self._build()))

    def _build(self):
        out = {
            "build-system": {
                "requires": self.buildSystem,
                "build-backend": self.buildSystemBackend,
            },
            "project": {
                "name": self.moduleName,
                "version": self.moduleVersion,
                "authors": self.authors,
                "description": self.description,
                "readme": self.readme,
                "requires-python": self.requiredPythonVersion,
                "dependencies": self.dependencies,
                "classifiers": [
                    "Programming Language :: Python :: 3",
                    f"License :: OSI Approved :: {self.license}",
                    f"Operating System :: {self.operatingSystem}",
                ],
                "urls": {
                    "Homepage": self.homepage,
                    "Bug Tracker": self.bugTracker,
                },
            },
        }
        return out

    def save(self):
        if self.savePath:
            savePath = self.savePath
        else:
            savePath = os.getcwd()

        path = os.path.join(savePath, "pyproject.toml")

        with open(path, mode="wb") as file:
            dump(self._build(), file)

    def preview(self):
        return dumps(self._build())


# mod = ModuleTOMLCreator(
#     buildSystem=["hatchling"],
#     buildSystemBackend="hatchling.build",
#     moduleName="mod_template",
#     moduleVersion="0.0.1",
#     moduleAuthors=[{"name": "travis", "email": "travis@trainland.io"}],
#     description="module for creating a template for basic python modules",
#     readme="README.md",
#     requiredPythonVersion=">=3.7",
#     dependencies=["tomli_w == 1.0.0"],
#     license="MIT License",
#     operatingSystem="OS Independent",
#     homepage="https://gitlab.com/travisbaars/mod-template",
#     bugTracker="https://gitlab.com/travisbaars/mod-template/issues",
# )

# print(mod.preview())
# print(get)
# mod.save()
