from colorama import Fore, Back, Style

from .utility import LICENSE_OPTIONS, OS_OPTIONS, BUILD_OPTIONS


class UserInput:
    def __init__(
        self,
        module_version=None,
        authors=None,
        required_python=None,
        readme=None,
        license=None,
        ignore=None,
        repository=None,
        build_system=None,
        operating_system=None,
    ) -> None:
        self.module_version_state = module_version
        self.authors_state = authors
        self.required_python_state = required_python
        self.readme_state = readme
        self.license_state = license
        self.ignore_state = ignore
        self.repository_state = repository
        self.build_system_state = build_system
        self.operating_system_state = operating_system

        self.license_options = LICENSE_OPTIONS
        self.os_options = OS_OPTIONS
        self.build_system_options = BUILD_OPTIONS
        self.authors = []
        self.dependencies = []
        # self.inputs = {}

    def run(self):
        # self.inputs = {"projectName": self._single_option("Module Name", None)}
        self.moduleName = self._single_option("Module Name", None)
        self.moduleVersion = (
            self.module_version_state
            if self.module_version_state
            else self._single_option("Module Version", "0.0.1")
        )

        if self.authors_state:
            self.authors = self.authors_state
        else:
            self.numberOfAuthors = self._single_option("Number of Authors", "1")
            self._get_authors(self.numberOfAuthors)

        self.description = self._single_option("Description", None)
        self.readme = (
            self.readme_state
            if self.readme_state
            else self._single_option("Readme", "README.md")
        )
        self.requiredPythonVersion = (
            self.required_python_state
            if self.required_python_state
            else self._single_option("Required Python Version", ">=3.7")
        )

        self.rawDependencies = self._single_option("Dependencies", "none")
        self._get_dependencies(self.rawDependencies)

        self.ignore = (
            self.ignore_state
            if self.ignore_state
            else self._single_option("Dependencies", "none")
        )

        self.license = (
            self.license_state
            if self.license_state
            else self._multi_option(self.license_options, "1")
        )
        self.operatingSystem = (
            self.operating_system_state
            if self.operating_system_state
            else self._multi_option(self.os_options, "1")
        )

        self.homepage = (
            self.repository_state[0]
            if self.repository_state
            else self._single_option("Homepage", None)
        )
        self.bugTracker = (
            self.repository_state[1]
            if self.repository_state
            else self._single_option("Bug Tracker", None)
        )

        self.build_system = (
            self.build_system_state
            if self.build_system_state
            else self._multi_option("Build System", "1")
        )

        self.inputs = {
            "Module Name": self.moduleName,
            "Module Version": self.moduleVersion,
            "Authors": self.authors,
            "Description": self.description,
            "Readme": self.readme,
            "Required Python": self.requiredPythonVersion,
            "Dependencies": self.dependencies,
            "Ignore": self.ignore,
            "License": self.license,
            "Operating System": self.operatingSystem,
            "Homepage": self.homepage,
            "Bug Tracker": self.bugTracker,
            "Build System": self.build_system,
        }

        return self.inputs

    def _normalize(self, x):
        normal = x.lower().replace(" ", "_")
        return normal

    def _get_authors(self, num):
        for count in range(int(num)):
            name = self._single_option(f"  Name  {count + 1}", None)
            email = self._single_option(f"  Email {count + 1}", None)

            self.authors.append({"name": name, "email": email})

        return None

    def _get_dependencies(self, raw):
        if raw == "none":
            return None
        split = raw.split(" ")

        for dep in split:
            fix = dep.replace("=", " == ")

            self.dependencies.append(fix)

        return None

    def _many_to_list(self, raw):
        collect = []
        if raw == "none":
            return None
        split = raw.split(" ")

        for dep in split:
            fix = dep.replace("=", " == ")

            collect.append(fix)

        return collect

    def _single_option(self, option, default):
        count = 0
        while True:
            if count == 3:
                raise ValueError("no valid input detected")
            if not default == None:
                text = (
                    f"{option}:"
                    + Style.DIM
                    + f"[default = {default}] "
                    + Style.RESET_ALL
                )
                choice = input(text)
                if choice == "":
                    return default
                else:
                    return choice
            else:
                text = f"{option}: "
                choice = input(text)

            if choice:
                # self.inputs[option] = choice
                return choice
            else:
                #     # catch = re.sub(r"[ 0-9]", "", option)
                print(Fore.RED + f"please provide an input" + Fore.RESET)
                # print("please provide an input")
                count += 1
                # raise Exception("please provide an input")

    def _multi_option(solf, options: dict, default: str):
        if not default:
            raise ValueError("must set a default value")
        while True:
            print("Select an option:")
            for key, value in options.items():
                print(f"{key}: {value}")
            choice = input(
                "Enter the number of your choice:"
                + Style.DIM
                + "[default = 1] "
                + Style.RESET_ALL
            )

            if choice in options:
                return options[choice]
            elif choice == "":
                return options[default]
            else:
                print(Fore.RED + "Invalid choice. Please try again." + Fore.RESET)


# _collect = _UserInput()


# def get_user_input():
#     return _collect._run()


if __name__ == "__main__":
    # print(get_user_input())

    _collect = UserInput()

    # get_user_input()
