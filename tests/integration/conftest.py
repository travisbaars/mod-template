import pytest
from mod_template.user_input import UserInput


@pytest.fixture(scope="function")
def init_UserInput():
    return UserInput(dependencies=True)


@pytest.fixture(scope="function")
def sample_input():
    inputs = iter(
        [
            "Fake Module",
            "0.1.0",
            "1",
            "sara",
            "sara@email",
            "simple description",
            "readme.md",
            ">=3.11",
            "fakepack demo=1.0.2",
            "2",
            "2",
            "home.com",
            "home.com/tracker",
            "1",
        ]
    )
    return inputs


@pytest.fixture(scope="function")
def sample_input_defaults():
    inputs = iter(
        [
            "Fake Module",
            "",
            "",
            "sara",
            "sara@email",
            "simple description",
            "",
            "",
            "",
            "",
            "",
            "home.com",
            "home.com/tracker",
            "",
        ]
    )
    return inputs


@pytest.fixture(scope="function")
def sample_pass():
    tool = {
        "Module Name": "Fake Module",
        "Module Version": "0.1.0",
        "Authors": [{"name": "sara", "email": "sara@email"}],
        "Description": "simple description",
        "Readme": "readme.md",
        "Required Python Version": ">=3.11",
        "Dependencies": ["fakepack", "demo == 1.0.2"],
        "License": "YUP",
        "Operating System": "Linux",
        "Homepage": "home.com",
        "Bug Tracker": "home.com/tracker",
        "Build System": "hatchling",
    }

    return tool


@pytest.fixture(scope="function")
def sample_pass_defaults():
    tool = {
        "Module Name": "Fake Module",
        "Module Version": "0.0.1",
        "Authors": [{"name": "sara", "email": "sara@email"}],
        "Description": "simple description",
        "Readme": "README.md",
        "Required Python Version": ">=3.7",
        "Dependencies": [],
        "License": "MIT",
        "Operating System": "OS Independent",
        "Homepage": "home.com",
        "Bug Tracker": "home.com/tracker",
        "Build System": "hatchling",
    }

    return tool
