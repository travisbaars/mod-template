# from mod_template.user_input import get_user_input


def test_get_user_input_no_defaults(
    init_UserInput, monkeypatch, sample_pass, sample_input
):
    monkeypatch.setattr("builtins.input", lambda _: next(sample_input))
    res = init_UserInput.run()
    print(res)

    assert res == sample_pass


def test_get_user_input_defaults(
    init_UserInput, monkeypatch, sample_pass_defaults, sample_input_defaults
):
    monkeypatch.setattr("builtins.input", lambda _: next(sample_input_defaults))
    res = init_UserInput.run()
    print(res)

    assert res == sample_pass_defaults
