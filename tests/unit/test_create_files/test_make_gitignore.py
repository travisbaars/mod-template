import os


def test_make_gitignore(
    init_CreateTemplateFiles, gitignore_defaults, gitignore_pass, tmp_path
):
    path = tmp_path / "working_dir/.gitignore"

    init_CreateTemplateFiles.make_gitignore(gitignore_defaults)
    with open(path, "r") as file:
        read = file.read()
    assert os.path.exists(path)
    assert read == gitignore_pass


def test_make_gitignore_empty(init_CreateTemplateFiles, tmp_path):
    path = tmp_path / "working_dir/.gitignore"

    init_CreateTemplateFiles.make_gitignore([])
    with open(path, "r") as file:
        read = file.read()
    assert os.path.exists(path)
    assert read == ""
