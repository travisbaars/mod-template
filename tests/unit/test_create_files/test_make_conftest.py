import os
import pytest


def test_make_conftest(init_CreateTemplateFiles, tmp_path):
    path1 = tmp_path / "working_dir/tests"
    path2 = tmp_path / "working_dir/tests/unit"
    path3 = tmp_path / "working_dir/tests/integration"

    paths = [path1, path2, path3]

    count = 0
    for path in paths:
        path.mkdir()
        count += 1
        if count == 3:
            init_CreateTemplateFiles.make_conftest()

    for path in paths:
        assert os.path.exists(os.path.join(path, "conftest.py"))


def test_make_conftest_error(init_CreateTemplateFiles):
    with pytest.raises(Exception, match="directory is non-existent"):
        init_CreateTemplateFiles.make_conftest()
