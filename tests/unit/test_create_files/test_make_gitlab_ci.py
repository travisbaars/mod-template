def test_make_gitlab_ci(init_CreateTemplateFiles, tmp_path):
    init_CreateTemplateFiles.make_gitlab_ci()
    assert (tmp_path / "working_dir/.gitlab-ci.yml").exists()
