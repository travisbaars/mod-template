import os


def test_make_src_directory(init_CreateTemplateFiles, tmp_path):
    tests_path = tmp_path / "working_dir/tests"
    unit_path = tests_path / "unit"
    integration_path = tests_path / "integration"

    assert init_CreateTemplateFiles.make_tests_directory()
    assert os.path.exists(tests_path)
    assert os.path.exists(unit_path)
    assert os.path.exists(integration_path)
