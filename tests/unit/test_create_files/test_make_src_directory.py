def test_make_src_directory(init_CreateTemplateFiles, tmp_path):
    assert init_CreateTemplateFiles.make_src_directory("Fake Module")
    assert (tmp_path / "working_dir/src/fake_module").exists()
