import os


def test_make_requirements(
    init_CreateTemplateFiles, requirements_list, requirements_pass, tmp_path
):
    filepath = tmp_path / "working_dir/requirements.txt"
    init_CreateTemplateFiles.make_requirements(requirements_list)
    with open(filepath) as file:
        read = file.read()
    assert os.path.exists(filepath)
    assert read == requirements_pass


def test_make_requirements_empty(init_CreateTemplateFiles, tmp_path):
    filepath = tmp_path / "working_dir/requirements.txt"
    init_CreateTemplateFiles.make_requirements([])
    with open(filepath) as file:
        read = file.read()
    assert os.path.exists(filepath)
    assert read == ""
