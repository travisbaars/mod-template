def test_make_readme(init_CreateTemplateFiles, tmp_path):
    init_CreateTemplateFiles.make_readme("README.md")
    assert (tmp_path / "working_dir/README.md").exists()
