import os
import pytest


def test_check_path_nopath(init_CreateTemplateFiles_nopath):
    cwd = os.getcwd()
    assert init_CreateTemplateFiles_nopath._check_path(cwd) == cwd


def test_check_path(init_CreateTemplateFiles, tmp_path):
    assert (
        init_CreateTemplateFiles._check_path(tmp_path / "working_dir")
        == tmp_path / "working_dir"
    )


def test_check_path_invalid(init_CreateTemplateFiles):
    with pytest.raises(ValueError, match="given directory path doesn't exist"):
        init_CreateTemplateFiles._check_path("/notarealpath")
    # print(err)
