import os


def test_make_license(init_CreateTemplateFiles, license_MIT_pass):
    test = init_CreateTemplateFiles.make_license("MIT", "John Doe")
    assert test == license_MIT_pass


def test_make_license_read(init_CreateTemplateFiles, license_MIT_pass, tmp_path):
    path = tmp_path / "working_dir/LICENSE"
    init_CreateTemplateFiles.make_license("MIT", "John Doe")

    with open(path, "r") as file:
        read = file.read()
    assert os.path.exists(path)
    assert read == license_MIT_pass
