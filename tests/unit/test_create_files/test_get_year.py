from datetime import date


def test_get_year(init_CreateTemplateFiles):
    today = str(date.today())

    assert init_CreateTemplateFiles._get_year() in today
