import pytest


license_options = {"1": "MIT", "2": "YUP"}


def test_multi_option_no_default_no_input(monkeypatch, init_UserInput):
    while True:
        monkeypatch.setattr("builtins.input", lambda _: "")
        with pytest.raises(ValueError) as err:
            init_UserInput._multi_option(license_options, None)
        break

    assert str(err.value) == "must set a default value"


def test_multi_option_default_no_input(monkeypatch, init_UserInput):
    monkeypatch.setattr("builtins.input", lambda _: "")
    assert init_UserInput._multi_option(license_options, "1") == "MIT"


def test_multi_option_default_input(monkeypatch, init_UserInput):
    monkeypatch.setattr("builtins.input", lambda _: "2")
    assert init_UserInput._multi_option(license_options, "1") == "YUP"
