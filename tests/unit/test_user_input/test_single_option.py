import pytest


def test_single_option_pass_no_default_no_input(capfd, monkeypatch, init_UserInput):
    while True:
        monkeypatch.setattr("builtins.input", lambda _: "")
        with pytest.raises(ValueError) as err:
            init_UserInput._single_option("Name", None)
        break

    assert str(err.value) == "no valid input detected"


def test_single_option_pass_no_default_input(monkeypatch, init_UserInput):
    monkeypatch.setattr("builtins.input", lambda _: "Fake Module")
    assert init_UserInput._single_option("Name", None) == "Fake Module"


def test_single_option_default_no_input(monkeypatch, init_UserInput):
    monkeypatch.setattr("builtins.input", lambda _: "")
    assert init_UserInput._single_option("Fake Version", "0.0.1") == "0.0.1"


def test_single_option_default_input(monkeypatch, init_UserInput):
    monkeypatch.setattr("builtins.input", lambda _: "1.0.0")
    assert init_UserInput._single_option("Fake Version", "0.0.1") == "1.0.0"
    assert not init_UserInput._single_option("Fake Version", "0.0.1") == "0.0.1"
