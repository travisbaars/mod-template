import pytest

from mod_template.user_input import UserInput
from mod_template.create_files import CreateTemplateFiles


@pytest.fixture
def init_UserInput():
    return UserInput(dependencies=True)


@pytest.fixture
def init_CreateTemplateFiles(tmp_path):
    path = tmp_path / "working_dir"
    path.mkdir()
    return CreateTemplateFiles(path=path)


@pytest.fixture
def init_CreateTemplateFiles_nopath():
    return CreateTemplateFiles()


# @pytest.fixture
# def init_CreateTemplateFiles_nodir():
#     return CreateTemplateFiles(path="/notarealpath")


# @pytest.fixture
# def init_CreateTemplateFiles_file(tmp_path):
#     dir = tmp_path / "working_directory"
#     dir.mkdir()
#     file = dir / "test.txt"
#     return CreateTemplateFiles(path=file)


@pytest.fixture()
def license_MIT_pass():
    return """
Copyright 2023 John Doe

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


@pytest.fixture
def requirements_list():
    req = ["fake == 0.1.1", "not_real == 2.2.1"]
    return req


@pytest.fixture
def requirements_empty_list():
    req = []
    return req


@pytest.fixture
def requirements_pass():
    return "fake == 0.1.1\nnot_real == 2.2.1\n"


@pytest.fixture
def requirements_empty_pass():
    return ""


@pytest.fixture
def gitignore_defaults():
    return [".venv", ".env", "dist"]


@pytest.fixture
def gitignore_pass():
    return ".venv\n.env\ndist\n"
